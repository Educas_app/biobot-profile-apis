from django.apps import AppConfig


class ProfileApisConfig(AppConfig):
    name = 'profile_apis'
