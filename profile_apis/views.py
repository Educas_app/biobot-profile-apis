from django.shortcuts import render
from profile_apis import models
from rest_framework import viewsets
from profile_apis import serializers
from rest_framework.authentication import TokenAuthentication
from profile_apis import permissions
from rest_framework import generics
from rest_framework.views import APIView
from profile_apis.models import patient_profile1,patient_med
from rest_framework import filters
# Create your views here.

class userprofileviewset(viewsets.ModelViewSet):
    serializer_class=serializers.userprofileserializer
    queryset = models.UserProfile.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.updateownplrofile,)



from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings

class UserLoginApiView(ObtainAuthToken):
   """Handle creating user authentication tokens"""
   renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES


class patientviewset(viewsets.ModelViewSet):
    queryset = patient_profile1.objects.all()
    serializer_class = serializers.patientserializer    
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name',)

class patient_medviewset(viewsets.ModelViewSet):
    queryset = patient_med.objects.all()
    serializer_class = serializers.patient_medserializer    
    filter_backends = (filters.SearchFilter,)
    search_fields = ('first_name',)    