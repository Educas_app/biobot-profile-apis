from django.urls import path, include
from rest_framework.routers import DefaultRouter
from profile_apis import views

router= DefaultRouter()
router.register('profile',views.userprofileviewset)
router.register('patient',views.patientviewset)
router.register('patient_med',views.patient_medviewset)



urlpatterns = [
    path('', include(router.urls)),
    path('login/',views.UserLoginApiView.as_view()),

]
