## user profile serializer

from rest_framework import serializers
from profile_apis import models



class userprofileserializer(serializers.ModelSerializer):

    class Meta:
        model = models.UserProfile
        fields = ('id','name','email','password')
        extra_kwargs={
            'password':{
                'write_only': True,
                'style':{'input_type':'password'}                
            }
        }

    def create(self, validated_data):
        """Create and return a new user"""
        user = models.UserProfile.objects.create_user(
            email=validated_data['email'],
            name=validated_data['name'],
            password=validated_data['password']
        )

        return user


class patientserializer(serializers.ModelSerializer):
    class Meta:
        model = models.patient_profile1
        fields = ('id','first_name','last_name','age','race','gender','smoking','marital_status','insurance','zipcode','height','bmi','weight','user_id')


class patient_medserializer(serializers.ModelSerializer):
    class Meta:
        model = models.patient_med
        fields = ('id','patient_id','user_id','Procedure','asa_status','co_morbidities')