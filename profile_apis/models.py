from django.db import models
from django.utils.crypto import get_random_string
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from datetime import datetime
from django.conf import settings
 
# Create your models here.

def user_id_generater():
    from django.utils.crypto import get_random_string
    return ''.join(get_random_string(length=5))

class UserProfileManager(BaseUserManager):

    def create_user(self,email,name,password=None):
        if not email:
            raise ValueError('email is required')
        email =self.normalize_email(email)
        user = self.model(email=email,name=name)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name,password):

        user=self.create_user(email,name, password)
        user.is_superuser=True    
        user.is_staff = True
        user.save(using=self._db)
        return user


class UserProfile(AbstractBaseUser,PermissionsMixin):
    email=models.EmailField(max_length=50,unique=True)
    name=models.CharField(max_length=250)
    is_staff=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    user_id=models.CharField(max_length=5,default=user_id_generater,unique=True)



    objects=UserProfileManager()

    USERNAME_FIELD='email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def __str__(self):
       return self.user_id            


    
class patient_profile1(models.Model):
    user_id= models.ForeignKey(UserProfile,on_delete=models.CASCADE)
    patient_id = models.CharField(default=user_id_generater,max_length=250,unique=True)
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    age  = models.IntegerField()
    race = models.CharField(max_length=250)
    gender = models.CharField(max_length=250)
    smoking = models.CharField(max_length=250)
    marital_status = models.CharField(max_length=250)
    insurance = models.CharField(max_length=250)
    zipcode = models.CharField(max_length=250)
    bmi = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    modified_by = models.CharField(max_length=250)
    def __str__(self):
       return self.patient_id  


class patient_med(models.Model):
    patient_id = models.ForeignKey(patient_profile1,on_delete=models.CASCADE)
    user_id = models.ForeignKey(UserProfile,on_delete=models.CASCADE)
    Procedure = models.CharField(max_length=250)
    co_morbidities = models.CharField(max_length=250)
    asa_status = models.CharField(max_length=250)

